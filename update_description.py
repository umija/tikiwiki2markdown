#!/usr/bin/env python
# -*- coding: utf-8 -*-
# encoding=utf8

import os
import ConfigParser
import sys
import mysql.connector
import time
import re


if len(sys.argv) > 1:
    sourcepath = sys.argv[1]
else:
    sourcepath = 'source/'

config = ConfigParser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

separator = config.get("TIKI", "namespace-separator")
committer = config.get("GIT", "committer")
committerEmail = config.get("GIT", "committer-email")
ext = config.get("TIKI", "filename-ext")



try:
    conn = mysql.connector.connect(
      host      = config.get('MYSQL-DB', 'host'),
      user      = config.get('MYSQL-DB', 'user'),
      passwd    = config.get('MYSQL-DB', 'passwd'),
      db        = config.get('MYSQL-DB', 'db'),
      charset   = config.get('MYSQL-DB', 'charset'),
      collation = config.get('MYSQL-DB', 'collation')
    )
except mysql.connector.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)



cur = conn.cursor()

try:
  cur.execute("""
      select
        pageName,
        description
      from
      tikiwiki.tiki_pages ;"""
  )
except MySQLdb.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)

for (pageName, description) in cur:
  pageName = re.sub(separator + '{2,}', separator, pageName)
  if (pageName[0] == separator):
    pageName = pageName.replace(separator, "_", 1)
  filepath = pageName.replace(separator, "/" ).replace(" ", "_" ) + ext


  filepath = os.path.join(sourcepath, filepath)


  if os.path.isfile(filepath):
      with open(filepath, 'r+') as file:
          content = file.read()
          content = u"#".encode('utf-8') + description.encode('utf-8') + u"\n".encode('utf-8') + content
          file.seek(0)
          file.write( content )


cur.close()
conn.close()
