The Tiki project is moving to Markdown. Please see: [doc.tiki.org/Tiki-Flavored-Markdown](https://doc.tiki.org/Tiki-Flavored-Markdown)


# tikiwiki2markdown

__tikiwiki2markdown__ remodels [tikiwiki](https://tiki.org) wiki syntax to markdown.


* `mysql2file.py` additionally creates wiki files from tikiwiki database
* `tiki2md.py` remodels syntax
* `tikihist2git.py` imports the history from tiki to git

## HowTo use

### tikihist2git
This scipit need some config parameter, ensure that you copy the example config file:
``` bash
cp config.default.ini config.ini
```
after this use your texteditor to enter the parameters to the config file.

Once you finished the setup you need to invoke the script and pipeline the output to the `git fast-import`. The following linke will create a temp folder, initiate an empty gir repository and execute the script with the pipe to `git`. You would need to start the below code from your cloned `tikiwiki2markdown` repository:
``` bash
mkdir -p source && cd ./source/ && v=`mktemp -d -p.` && cd $v && git init && python ../../tikihist2git.py | git fast-import && git checkout && cd ../..
```

## TODO

* list needs an leading blank line in markdown
