#!/usr/bin/env python
# -*- coding: utf-8 -*-
# encoding=utf8

import os
import ConfigParser
import sys
import mysql.connector
import time
import re

config = ConfigParser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))
filepath = []

separator = config.get("TIKI", "namespace-separator")
committer = config.get("GIT", "committer")
committerEmail = config.get("GIT", "committer-email")
ext = config.get("TIKI", "filename-ext")

try:
    conn = mysql.connector.connect(
      host      = config.get('MYSQL-DB', 'host'),
      user      = config.get('MYSQL-DB', 'user'),
      passwd    = config.get('MYSQL-DB', 'passwd'),
      db        = config.get('MYSQL-DB', 'db'),
      charset   = config.get('MYSQL-DB', 'charset'),
      collation = config.get('MYSQL-DB', 'collation')
    )
except mysql.connector.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)



cur = conn.cursor()

try:
  cur.execute("""
    select
    pageName
    from tikiwiki.tiki_pages p,
    tikiwiki.users_objectpermissions perm
    where 1=1
    and md5(concat('wiki page', p.pageName)) = perm.objectId
    and perm.objectType = 'wiki page'
    and perm.permName = 'tiki_p_view'
    group by pageName;
    """)
except MySQLdb.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)

for pageName in cur:
    print pageName[0]
    pageName = re.sub(separator + '{2,}', separator, pageName[0])

    if (pageName[0] == separator):
        pageName = pageName.replace(separator, "_", 1)

    filepath.append(pageName.replace(separator, "/" ).replace(" ", "_" ) + ext)



cur.close()
conn.close()

with open('restrictedpages.txt', 'wb') as fp_restricted:
    fp_restricted.write("\n".join(filepath))
