-- PageView Einschränkungen pro Gruppe

-- erzeugt eine liste ~289 mit angepassten Seitenrechten

-- ~20030006 	buddies
-- ~20030006 	CPK
-- ~20049051:2005-03-15 	Admins
-- ~20049061 	buddies
-- ~20050009:2005-08-07 	Admins
-- ~20050010 	Anonymous
-- ~20050023 	Registered
-- ~20050024 	Anonymous
-- ~20050031 	buddies
-- diese seiten kann man ohne die Anonymous mit dem tag publish: false taggen 

select 
p.pageName, perm.groupName
from tikiwiki.tiki_pages p,
tikiwiki.users_objectpermissions perm
where 1=1
and md5(concat('wiki page', p.pageName)) = perm.objectId 
and perm.objectType = "wiki page"
and perm.permName = "tiki_p_view"
;


-- Resultat nicht eindeutig (Seite kann mehrmals gelistet werden"
select 
count(pageName) "Anzahl Seiten", count(distinct pageName) "Anzahl eindeutiger Seiten"
from tikiwiki.tiki_pages p,
tikiwiki.users_objectpermissions perm
where 1=1
and md5(concat('wiki page', p.pageName)) = perm.objectId 
and perm.objectType = "wiki page"
and perm.permName = "tiki_p_view"
;

-- Diese Seiten sind mehrmals gelistet
select 
pageName, count(1)
from tikiwiki.tiki_pages p,
tikiwiki.users_objectpermissions perm
where 1=1
and md5(concat('wiki page', p.pageName)) = perm.objectId 
and perm.objectType = "wiki page"
and perm.permName = "tiki_p_view"
group by pageName
having count(1) > 1
;


-- Mehrfachlisting, da eine Seite mehrere Berechtigungen haben kann.
-- Beispielseiten mit Berechtigungen für drei Gruppen
select 
p.pageName, perm.groupName
from tikiwiki.tiki_pages p,
tikiwiki.users_objectpermissions perm
where 1=1
and md5(concat('wiki page', p.pageName)) = perm.objectId 
and perm.objectType = "wiki page"
and perm.permName = "tiki_p_view"
and p.pageName = "sulu:fh"
;