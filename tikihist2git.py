#!/usr/bin/env python
# -*- coding: utf-8 -*-
# encoding=utf8

import os
import ConfigParser
import sys
import mysql.connector
import time
import re

config = ConfigParser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

separator = config.get("TIKI", "namespace-separator")
committer = config.get("GIT", "committer")
committerEmail = config.get("GIT", "committer-email")
ext = config.get("TIKI", "filename-ext")

try:
    conn = mysql.connector.connect(
      host      = config.get('MYSQL-DB', 'host'),
      user      = config.get('MYSQL-DB', 'user'),
      passwd    = config.get('MYSQL-DB', 'passwd'),
      db        = config.get('MYSQL-DB', 'db'),
      charset   = config.get('MYSQL-DB', 'charset'),
      collation = config.get('MYSQL-DB', 'collation')
    )
except mysql.connector.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)



cur = conn.cursor()

try:
  cur.execute("""
    select
      pageName,
      description,
      lastModif,
      data,
      user,
      version,
      COALESCE(comment, '') as comment,
      u.email
    from
    (
      select
        pageName,
        description,
        lastModif,
        data,
        version,
        user,
        comment
      from
      tikiwiki.tiki_pages p
      union all
      select
        pageName,
        description,
        lastModif,
        CONVERT(data USING utf8) as data,
        version,
        user,
        comment
      from
      tikiwiki.tiki_history h
    ) c
    left join
    tikiwiki.users_users u on (c.user=u.login)
    order by lastModif, pageName;"""
  )
except MySQLdb.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)

for (pageName, description, lastModif, data, user, version, comment, email) in cur:
  pageName = re.sub(separator + '{2,}', separator, pageName)
  if (pageName[0] == separator):
    pageName = pageName.replace(separator, "_", 1)
  filepath = pageName.replace(separator, "/" ).replace(" ", "_" ) + ext
  commitTime = int(time.time())
  if (comment == ''):
    comment = str(version) + '. Version der Seite ' + filepath
  if (data is None):
    data =''
  if (email is None):
    email = 'none'
  if (lastModif is None):
    lastModif = 1

  print("commit refs/heads/master")
  print("author {} <{}> {:d} +0000".format(user.encode('utf-8'), email.encode('utf-8'), lastModif))
  print("committer {} <{}> {:d} +0000".format(committer.encode('utf-8'), committerEmail.encode('utf-8'), commitTime))
  print("data {:d}".format(len(comment.encode('utf-8'))))
  print(comment.encode('utf-8'))
  print("M 644 inline {}".format(filepath.encode('utf-8')))
  print("data {:d}".format(len(data.encode('utf-8'))))
  print(data.encode('utf-8'))

cur.close()
conn.close()
